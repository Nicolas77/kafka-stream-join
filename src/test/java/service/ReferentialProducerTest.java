package service;

import domain.bean.Referential;
import org.junit.Test;
import producer.ReferentialProducer;
import util.DataFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

// this test need a kafka broker - not still declare in maven
public class ReferentialProducerTest {


    @Test
    public void sendOneReferential() throws ExecutionException, InterruptedException {
        Referential referential = DataFactory.createReferential(1, new Double("10.30"));
        ReferentialProducer producer = new ReferentialProducer();
        producer.sendOne(referential);

    }


    @Test
    public void sendMultipleReferential() throws ExecutionException, InterruptedException {
        List referentials = new ArrayList<>();
        referentials.add(DataFactory.createReferential(1, new Double("10.10")));
        referentials.add(DataFactory.createReferential(2, new Double("20.20")));

        ReferentialProducer producer = new ReferentialProducer();
        producer.sendMultiple(referentials);

    }

}
