package util;

import bean.MyProduct;
import bean.MyOrder;
import bean.MyReferential;
import domain.bean.Product;
import domain.bean.Order;
import domain.bean.Referential;

import java.util.ArrayList;
import java.util.List;

public class DataFactory {

    public static Order createOrder(int index) {
        String id = "ORD-" + index;
        String label = "label for order " + index;
        List<Product> products = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            products.add(createProduct(i));
        }
        Order product = new MyOrder(id, label, products);
        return product;
    }

    private static Product createProduct(int index) {
        String id = "ID-" + index;
        String label = "label product for product " + index;
        String referentialKey = "REF-" + index;
        Product product = new MyProduct(id, label, referentialKey);
        return product;
    }

    public static Referential createReferential(int index, Double price) {
        String key = "REF-" + index;
        Referential referential = new MyReferential(key, price);
        return referential;
    }
}
