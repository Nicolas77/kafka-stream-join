package bean;

import domain.bean.Product;

public class MyProduct implements Product {

    private final String id;

    private final String label;

    private final String referentialKey;

    public MyProduct(String id, String label, String referentialKey) {
        this.id = id;
        this.label = label;
        this.referentialKey = referentialKey;
    }

    @Override
    public String getJoinKey() {
        return referentialKey;
    }
}
