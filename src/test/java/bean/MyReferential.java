package bean;

import domain.bean.Referential;

public class MyReferential implements Referential {

    private final String key;

    private final Double price;

    public MyReferential(String key, Double price) {
        this.key = key;
        this.price = price;
    }

    @Override
    public String getKey() {
        return key;
    }

    public Double getPrice() {
        return price;
    }
}
