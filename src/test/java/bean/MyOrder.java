package bean;

import domain.bean.Product;
import domain.bean.Order;

import java.util.List;

public class MyOrder implements Order {

    private final String id;

    private final String label;

    private final List<Product> products;

    public MyOrder(String id, String label, List<Product> products) {
        this.id = id;
        this.label = label;
        this.products = products;
    }

    @Override
    public List<Product> getProducts() {
        return products;
    }

}
