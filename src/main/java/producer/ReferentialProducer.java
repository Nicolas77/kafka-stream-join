package producer;

import domain.KafkaConfig;
import domain.bean.Referential;
import factory.MessageFactory;
import message.ReferentialMessage;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static domain.Schemas.Topics.REFERENTIEL;

public class ReferentialProducer {


    public void sendOne(Referential referential) throws ExecutionException, InterruptedException {
        Properties producerConfig = KafkaConfig.getProducerConfig();
        KafkaProducer<String, ReferentialMessage> producer = new KafkaProducer<>(producerConfig,
                REFERENTIEL.keySerde().serializer(),
                REFERENTIEL.valueSerde().serializer());

        ReferentialMessage message = MessageFactory.createReferentialMessage(referential);
        Future<RecordMetadata> record = producer.send(new ProducerRecord<>(REFERENTIEL.name(), message.getKey(), message));
        System.out.println("Message sent : " + record.get().offset());
    }

    public void sendMultiple(List<Referential> referentials) throws ExecutionException, InterruptedException {
        Properties producerConfig = KafkaConfig.getProducerConfig();
        KafkaProducer<String, ReferentialMessage> producer = new KafkaProducer<>(producerConfig,
                REFERENTIEL.keySerde().serializer(),
                REFERENTIEL.valueSerde().serializer());
        for(Referential referential : referentials) {
            ReferentialMessage message = MessageFactory.createReferentialMessage(referential);
            Future<RecordMetadata> record = producer.send(new ProducerRecord<>(REFERENTIEL.name(), message.getKey(), message));
            System.out.println("Message sent : " + record.get().offset());
        }
    }

}
