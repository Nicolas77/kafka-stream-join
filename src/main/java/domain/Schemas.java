package domain;

import com.fasterxml.jackson.databind.JsonNode;
import domain.deser.CustomSerializer;
import message.*;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.connect.json.JsonDeserializer;
import org.apache.kafka.connect.json.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

public class Schemas {


    public static class Topic<K, V> {

        private String name;
        private Serde<K> keySerde;
        private Serde<V> valueSerde;

        Topic(String name, Serde<K> keySerde, Serde<V> valueSerde) {
            this.name = name;
            this.keySerde = keySerde;
            this.valueSerde = valueSerde;
            Topics.ALL.put(name, this);
        }

        public Serde<K> keySerde() {
            return keySerde;
        }

        public Serde<V> valueSerde() {
            return valueSerde;
        }

        public String name() {
            return name;
        }

        public String toString() {
            return name;
        }
    }

    public static class Topics {

        public static Map<String, Topic> ALL = new HashMap<>();
        public static Topic<String, OrderMessage> ORDERS;
        public static Topic<String, ProductMessage> PRODUCTS;
        public static Topic<String, ReferentialMessage> REFERENTIEL;
        public static Topic<String, ProductMappedMessage> PRODUCT_MAPPED;

        public static Topic<String, OrderMappedMessage> ORDER_MAPPED;
        public static Topic<String, OrderMappedMessage> ORDER_NOT_MAPPED;

        static {
            createTopics();
        }

        private static void createTopics() {
            final Serializer jsonSerializer = new CustomSerializer();
            final Deserializer jsonDeserializer = new JsonDeserializer();
            final Serde jsonSerde = Serdes.serdeFrom(jsonSerializer, jsonDeserializer);


            ORDERS = new Topic("orders", Serdes.String(), jsonSerde);
            PRODUCTS = new Topic("products", Serdes.String(), jsonSerde);
            REFERENTIEL = new Topic("referential", Serdes.String(), jsonSerde);
            PRODUCT_MAPPED = new Topic("product-mapped", Serdes.String(), jsonSerde);
            ORDER_MAPPED = new Topic("order-mapped", Serdes.String(), jsonSerde);
            ORDER_NOT_MAPPED = new Topic("order-not-mapped", Serdes.String(), jsonSerde);

        }
    }

}
