package domain.bean;

import java.util.Optional;

public class ProductMapped {

    private Product detail;

    private Optional<Referential> referentiel;


    public ProductMapped(Product detail, Optional<Referential> referentiel) {
        this.detail = detail;
        this.referentiel = referentiel;
    }

    public Product getDetail() {
        return detail;
    }

    public Optional<Referential> getReferentiel() {
        return referentiel;
    }
}
