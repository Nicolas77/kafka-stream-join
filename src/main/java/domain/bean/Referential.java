package domain.bean;

public interface Referential {

    public String getKey();
}
