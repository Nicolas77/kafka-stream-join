package domain.bean;

import java.util.ArrayList;
import java.util.List;

public class OrderMapped {

    private String key;

    private List<ProductMapped> detailsMappeds;


    public OrderMapped() {
        this.detailsMappeds = new ArrayList<>();
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void addDataDetailsMapped(ProductMapped detailsMapped) {
        this.detailsMappeds.add(detailsMapped);
    }

    public String getKey() {
        return key;
    }

    public List<ProductMapped> getDetailsMappeds() {
        return detailsMappeds;
    }

    public boolean isComplete() {
        return this.detailsMappeds.stream().filter(b -> !b.getReferentiel().isPresent()).count() == 0;
    }

}
