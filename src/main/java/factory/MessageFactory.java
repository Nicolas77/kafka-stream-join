package factory;

import domain.bean.Order;
import domain.bean.Referential;
import message.OrderMessage;
import message.ProductMessage;
import message.ReferentialMessage;

import java.time.LocalDateTime;
import java.util.UUID;


public class MessageFactory {


    public static OrderMessage createOrderMessage(Order order) {
        OrderMessage orderMessage = new OrderMessage(order);
        final int nbElements = order.getProducts().size();

        String key = UUID.randomUUID().toString();
        final String aggregateKeyOrder = key; // generate a key for aggregatation

        order.getProducts().stream().forEach(detail -> {
            ProductMessage productMessage = new ProductMessage(detail, nbElements, aggregateKeyOrder);
            orderMessage.addDetailsMessage(productMessage);
        });
        return orderMessage;
    }

    public static ReferentialMessage createReferentialMessage(Referential referential) {
        ReferentialMessage message = new ReferentialMessage(referential);
        return message;

    }
}
