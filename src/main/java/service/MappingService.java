package service;

import domain.KafkaConfig;
import message.OrderMessage;
import message.ProductMappedMessage;
import message.ReferentialMessage;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;
import util.MicroserviceUtils;

import static domain.Schemas.Topics.*;

public class MappingService implements Service{

    private KafkaStreams streams;

    @Override
    public void start(String bootstrapServers) {
        streams = processStreams();
        streams.cleanUp(); //don't do this in prod as it clears your state stores
        streams.start();
    }

    public KafkaStreams processStreams() {
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, OrderMessage> orderMessageKStream = builder
                .stream(ORDERS.name(), Consumed.with(ORDERS.keySerde(), ORDERS.valueSerde()));


        KTable<String, ReferentialMessage> referentialMessageTable = builder.table(REFERENTIEL.name(), Consumed.with(REFERENTIEL.keySerde(), REFERENTIEL.valueSerde()));

        orderMessageKStream.flatMapValues(productMessage -> productMessage.getProductMessages())
                .map((k, v) -> new KeyValue<>(v.getJoinKey(), v))
                .through(PRODUCTS.name(), Produced.with(PRODUCTS.keySerde(), PRODUCTS.valueSerde()))
                .leftJoin(referentialMessageTable, (productMsg, referential) -> {
                    if (referential == null) {
                        return new ProductMappedMessage(productMsg);
                    } else {
                        return new ProductMappedMessage(productMsg, referential.getReferential());
                    }
                })
                .map((k,v) -> new KeyValue<>(v.getAggregateKeyOrder(), v))
                .to(PRODUCT_MAPPED.name(), Produced.with(PRODUCT_MAPPED.keySerde(), PRODUCT_MAPPED.valueSerde()));


        return new KafkaStreams(builder.build(), KafkaConfig.getStreamConfig("mapping"));
    }

    @Override
    public void stop() {
        if (streams != null) {
            streams.close();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Service mappingService = new MappingService();
        mappingService.start("");
        MicroserviceUtils.addShutdownHookAndBlock(mappingService);
    }
}
