package service;

public interface Service {

  void start(String bootstrapServers);

  void stop();
}