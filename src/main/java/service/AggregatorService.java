package service;

import domain.KafkaConfig;
import domain.bean.ProductMapped;
import message.OrderMappedMessage;
import message.ProductMappedMessage;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;

import static domain.Schemas.Topics.ORDER_MAPPED;
import static domain.Schemas.Topics.ORDER_NOT_MAPPED;
import static domain.Schemas.Topics.PRODUCT_MAPPED;

public class AggregatorService {


    public KafkaStreams service() {


        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, ProductMappedMessage> productMappedMessageKStream = builder
                .stream(PRODUCT_MAPPED.name(), Consumed.with(PRODUCT_MAPPED.keySerde(), PRODUCT_MAPPED.valueSerde()));


        KStream<String, OrderMappedMessage>[] branch = productMappedMessageKStream
                .groupByKey()
                .windowedBy(TimeWindows.of(1000 * 60 * 2))
                .aggregate(OrderMappedMessage::new,
                        (key, detailsMapped, productMapped) -> {
                            if (detailsMapped != null) {
                                productMapped.setNbElements(detailsMapped.getNbElements());
                                productMapped.getOrderMapped().setKey(detailsMapped.getAggregateKeyOrder());
                                ProductMapped detailMapped = new ProductMapped(detailsMapped.getProduct(), detailsMapped.getReferential());
                                productMapped.getOrderMapped().addDataDetailsMapped(detailMapped);
                            }
                            return productMapped;
                        },
                        Materialized.with(ORDER_MAPPED.keySerde(), ORDER_MAPPED.valueSerde()))
                .toStream((windowedKey, orderValue) -> windowedKey.key())
                .filter((k, v) -> v != null)//When elements are evicted from a session window they create delete events. Filter these out.
                .filter((k, v) -> v.allElementsArePresent())
                .branch((k, v) -> v.getOrderMapped().isComplete(),
                        (k, v) -> !v.getOrderMapped().isComplete());

        branch[0].to(ORDER_MAPPED.name(), Produced.with(ORDER_MAPPED.keySerde(), ORDER_MAPPED.valueSerde()));

        branch[1].to(ORDER_NOT_MAPPED.name(), Produced.with(ORDER_NOT_MAPPED.keySerde(), ORDER_NOT_MAPPED.valueSerde()));


        return new KafkaStreams(builder.build(), KafkaConfig.getStreamConfig("aggreg"));
    }

}
