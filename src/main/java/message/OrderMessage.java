package message;

import domain.bean.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderMessage {

    private Order order;

    private List<ProductMessage> productMessages;

    public OrderMessage(Order data) {
        this.order = data;
        this.productMessages = new ArrayList<ProductMessage>();
    }

    public void addDetailsMessage(ProductMessage detailsMessage) {
        this.productMessages.add(detailsMessage);
    }

    public Order getOrder() {
        return order;
    }

    public List<ProductMessage> getProductMessages() {
        return productMessages;
    }
}
