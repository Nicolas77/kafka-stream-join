package message;

import domain.bean.Product;

public class ProductMessage {

    private Product product;

    private int nbElements;

    private String aggregateKeyOrder;


    public ProductMessage(Product product, int nbElements, String aggregateKeyOrder) {
        this.product = product;
        this.nbElements = nbElements;
        this.aggregateKeyOrder = aggregateKeyOrder;
    }

    public String getJoinKey() {
        return this.product.getJoinKey();
    }

    public Product getProduct() {
        return product;
    }

    public int getNbElements() {
        return nbElements;
    }

    public String getAggregateKeyOrder() {
        return aggregateKeyOrder;
    }
}
