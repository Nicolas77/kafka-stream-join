package message;

import domain.bean.Referential;


public class ReferentialMessage {

    private Referential referential;

    private String key;

    public ReferentialMessage(Referential referential) {
        this.key = referential.getKey();
        this.referential = referential;
    }

    public ReferentialMessage(Referential referential, String key) {
        this.referential = referential;
        this.key = key;
    }

    public Referential getReferential() {
        return referential;
    }

    public String getKey() {
        return key;
    }

    public void setReferential(Referential referential) {
        this.referential = referential;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
