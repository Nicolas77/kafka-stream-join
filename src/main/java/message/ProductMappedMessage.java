package message;

import domain.bean.Referential;

import java.util.Optional;

public class ProductMappedMessage extends ProductMessage {


    private Optional<Referential> referential;


    public ProductMappedMessage(ProductMessage messageDetail, Referential referential) {
        super(messageDetail.getProduct(), messageDetail.getNbElements(), messageDetail.getAggregateKeyOrder());
        this.referential = Optional.of(referential);
    }

    public ProductMappedMessage(ProductMessage messageDetail) {
        super(messageDetail.getProduct(), messageDetail.getNbElements(), messageDetail.getAggregateKeyOrder());
        this.referential = Optional.empty();
    }

    public Optional<Referential> getReferential() {
        return referential;
    }
}
