package message;

import domain.bean.OrderMapped;

public class OrderMappedMessage {

    private OrderMapped orderMapped;

    private int nbElements;

    public OrderMappedMessage() {
        this.orderMapped = new OrderMapped();
    }

    public int getNbElements() {
        return nbElements;
    }

    public void setNbElements(int nbElements) {
        this.nbElements = nbElements;
    }

    public OrderMapped getOrderMapped() {
        return orderMapped;
    }

    public boolean allElementsArePresent() {
        return this.orderMapped.getDetailsMappeds().size() == nbElements;
    }

}
