# create topics
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic orders
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic products
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic referential
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic product-mapped
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic order-mapped
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic order-not-mapped


# describe topic
kafka-topics.sh --describe --zookeeper localhost:2181 --topic product

